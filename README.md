
## Martians Time Microservice

That microservice receive the time on Earth in UTC as an input and return two values:
the Mars Sol Date (MSD) and the Martian Coordinated Time (MTC).

## How to install
**Clone git repo**
git clone git@bitbucket.org:teise/billie-martian-time-service.git

**install libraries**
composer install

**run Laravel server**
php artisan serve - run laravel server

**set API_KEY in .env file**

## Request

GET http://127.0.0.1:8000/api/v1/martian-time?api_token=p2lbgWkFrykA4QyUmpHihzmc5BNzIABq&earth_time=01.12.2012

## Response

``{"success":true,"data":{"msd":49382.89941,"mtc":"21:35:09"},"message":"OK"}``

### Endpoint

/api/v1/martian-time

### Params
**api_token** - localed in .env API_KEY

**earth_time** - Earth date and time in UTC string format


### Routs
routes/api-v1.php

### Controllers
app/Http/Controllers/API/MartianTimeController.php

app/Http/Controllers/API/BaseController.php

### Service
app/Http/Services/MartianTimeService.php

### Tests
tests/Feature/MartianTimeApiTest.php
tests/Unit/MartianTimeServiceTest.php

### Run tests

./vendor/bin/phpunit tests/Unit/MartianTimeServiceTest.php --debug

./vendor/bin/phpunit tests/Feature/MartianTimeApiTest.php --debug