<?php

namespace App\Http\Controllers\API;


use App\Http\Requests\MartianTimeRequest;
use App\Http\Services\MartianTimeService;
use Illuminate\Http\Request;
use Illuminate\Validation\Validator;

/**
 * Class MartianTimeController
 * @package App\Http\Controllers\API
 */
final class MartianTimeController extends BaseController
{

    /**
     * @var MartianTimeService
     */
    private $martianTimeService;

    /**
     * MartianTimeController constructor.
     * @param MartianTimeService $martianTimeService
     */
    public function __construct(MartianTimeService $martianTimeService)
    {
        $this->martianTimeService = $martianTimeService;
    }

    /**
     * @param MartianTimeRequest $request
     * @return string
     */
    public function getMartianTime(MartianTimeRequest $request)
    {
        $request->validated();

        $earthTime = $request->input('earth_time');

        $data = $this->martianTimeService->getTime($earthTime);

        return $this->sendResponse($data, 'OK', 200);
    }
}