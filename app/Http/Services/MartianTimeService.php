<?php

namespace App\Http\Services;


final class MartianTimeService
{
    // one Martian day (sol) in seconds
    private const SOL = 88775.244;

    // agreement that Mars Sol Date (MSD) should be positive up to 29th December 1873
    private const AGREEMENT_FIGURE =  44796.0;

    // correction factor
    private const K = 0.0009626;

    // 6 January 2000 00:00:00 in Earth UTC seconds
    private const MARTIAN_ZERO_TIME = 947116800;

    /**
     * @param string $earthTime
     * @return array
     */
    public function getTime(string $earthTime): array {

        $earthTime = strtotime($earthTime);

        if (!$earthTime) {
            return [];
        }

        $msd = $this->getMarsSolDate($earthTime);
        $mtc = $this->getMartianCoordinatedTime($msd);

        return ['msd' => $msd, 'mtc' => $mtc];
    }

    /**
     * @param int $earthTime
     * @return float
     */
    private function getMarsSolDate(int $earthTime): float {

        $solsSinceZero = ($earthTime - self::MARTIAN_ZERO_TIME) / self::SOL;

        $msd = $solsSinceZero + self::AGREEMENT_FIGURE - self::K;

        return round($msd, 5);
    }

    /**
     * @param float $msd
     * @return string
     */
    private function getMartianCoordinatedTime(float $msd): string {

        $hours = fmod($msd,1) * 24;
        $mins = fmod($hours, 1) * 60;
        $secs = fmod($mins, 1) * 60;

        return sprintf('%02d:%02d:%02d', $hours, $mins, $secs);
    }
}