<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\MartianTimeController;

Route::get('/martian-time', [MartianTimeController::class, 'getMartianTime'])
    ->middleware('api_token');
