<?php

date_default_timezone_set('UTC');

$t = strtotime("14 Mar 2021, 00:01:22");


$sol = 88775.244; // one Mars's day in seconds

$agreementFigure =  44796.0; // agreement that Mars Sol Date (MSD) should be positive up to 29th December 1873

$k = 0.0009626; // holy math

$marsZeroTime = strtotime("6 January 2000"); // sec

$solsSinceZero = ($t - $marsZeroTime) / $sol;

$msd = $solsSinceZero + $agreementFigure - $k;

echo round($msd, 5) . PHP_EOL;


//$msd = 52326.9651;
//
//$hours = fmod($msd,1) * 24;
//$mins = fmod($hours, 1) * 60;
//$secs = fmod($mins, 1) * 60;
//
//$mtc = sprintf('%02d:%02d:%02d:', $hours, $mins, $secs);
//
//echo $mtc;

