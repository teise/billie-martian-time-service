<?php

namespace Tests\Unit;

use App\Http\Services\MartianTimeService;
use PHPUnit\Framework\TestCase;

/**
 * Class MartianTimeServiceTest
 * @package Tests\Unit
 */
final class MartianTimeServiceTest extends TestCase
{
    /**
     * @var MartianTimeService
     */
    private $martianTimeService;

    public function setUp(): void
    {
        $this->martianTimeService = app( MartianTimeService::class);

        parent::setUp();
    }

    /**
     * @dataProvider provider
     * @param $utc
     * @param $expected
     */
    public function testGetTime($utc, $expected)
    {
        $result = $this->martianTimeService->getTime($utc);
        $this->assertEquals($expected, $result);
    }

    /**
     * @return array
     */
    public function provider(): array {

        return [
            [
                'utc' => '14 Mar 2021, 16:09:18 UTC',
                'expected' => ['msd' => 52327.61853, 'mtc' => '14:50:40'],
            ],
            [
                'utc' => 'some random string',
                'expected' => [],
            ],
        ];
    }
}
