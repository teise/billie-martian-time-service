<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MartianTimeApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUnauthorizedRequest(): void
    {
        $response = $this->get('/api/v1/martian-time');

        $response->assertStatus(401);
        $response->assertUnauthorized();
    }

    public function testBadRequest() :void
    {
        $api_token = env('API_KEY');

        $response = $this->get('/api/v1/martian-time?api_token=' . $api_token);

        $response->assertStatus(422);
    }

    public function testSuccessRequest() :void
    {
        $api_token = env('API_KEY');

        $response = $this->get('/api/v1/martian-time?earth_time=2012-03-12&api_token=' . $api_token);

        $response->assertStatus(200);
        $response->assertOk();

        $response->assertJson(['success' => true, 'data' => ['msd' => 49125.96292, 'mtc' => '23:06:36'], 'message' => 'OK']);
    }
}
